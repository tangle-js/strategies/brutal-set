const Validator = require('is-my-json-valid')
const isEqual = require('lodash.isequal')
const Queue = require('./queue')

module.exports = function Overwrite (opts = {}) {
  const {
    keyPattern = '.+'
  } = opts

  const identity = {}
  // const reifiedIdentity = {}

  const schema = {
    type: 'object',
    patternProperties: {
      [keyPattern]: { type: 'boolean' }
    },
    additionalProperties: false
  }
  const isValid = Validator(schema, { verbose: true })

  function isIdentity (T) {
    return isEqual(T, identity)
  }

  function mapToOutput (T) {
    // if (isIdentity(T)) return reifiedIdentity
    return Object.entries(T).reduce((acc, [key, value]) => {
      if (value) acc.push(key)
      return acc
    }, [])
  }

  function concat (a, b) {
    if (!isValid(a)) throw ConcatError(a)
    if (!isValid(b)) throw ConcatError(b)

    if (isIdentity(b)) return a

    return { ...a, ...b }
  }

  function mapFromInput (change) {
    if (typeof change !== 'object') throw new Error(`invalid input ${change}`)
    else {
      if (change === null) throw new Error('invalid input "null"')
      if (!isValid(change)) throw new Error('invalid input', { cause: change })
      return change
    }
  }

  function isConflict (graph, tipIds, field, optSubFields, optResultRef = {}) {
    if (!Array.isArray(tipIds)) return new Error('tipIds should be an array')

    return false
  }

  function isValidMerge (graph, mergeNode, field) {
    return true
  }

  function merge (graph, mergeNode, field) {
    const tips = {
      queue: new Queue(),
      // nodes we're processing to find tips, made up of objects { key, T, d }, representing position in graph:
      //   - key = the unique identifier for a node
      //   - T = the accumulated (concat'd) Transformation up to and including node `key`
      //   - d = distance from root, in a merge it increases to be longer than the longest path
      preMerge: new Map()
      // a store for nodes from queue which are waiting for other nodes preceding a merge node being resolved,
      // these are held till all required nodes and states are ready
      // {
      //   [key]: { key, T },
      // }
    }

    const getT = (key) => {
      return graph.getNode(key).data[field]
    }
    graph.rootNodeKeys.forEach(key => {
      tips.queue.add({
        key,
        T: getT(key)
      })
    })

    let answer

    while (!answer || !tips.queue.isEmpty()) {
      const { key, T } = tips.queue.next()

      graph.getLinks(key).forEach(nextKey => {
        if (!graph.isMergeNode(nextKey)) {
          tips.queue.add({
            key: nextKey,
            T: concat(T, getT(nextKey))
          })
        // queue up the another node to explore from
        } else {
          tips.preMerge.set(key, { key, T })

          const requiredNodeIds = graph.getBacklinks(nextKey)
          const isMergeReady = requiredNodeIds.every(key => tips.preMerge.has(key))
          // check tips.preMerge store to see if we now have the state needed to complete merge

          if (isMergeReady) {
            const tipTs = requiredNodeIds.map(getT)
            const nextT = betterMerge(tipTs, getT(nextKey))

            if (nextKey === mergeNode.key) {
              answer = nextT
              // DONE!
            } else {
              tips.queue.add({
                key: nextKey,
                T: nextT
              })
            }
          }
        }
      })
    }

    return answer

    // TODO mix 2023-11-17
    // I'm pretty grumpy about the signature of the main "merge" function
    // There must be some reason that past me made such an ugly / stupid looking API
    // TODO investigate + refactor @tangle/overwrite OR @tangle/reduce ?

    // SURELY the signature should be something more like the following
    // (no "field", no "graph", no full nodes... ONLY the transformations?)
    function betterMerge (tipTs, mergeT) {
      // tips = [T]
      // mergeT = T

      // merge the tips, where false wins
      // then concat the merged tipTs with mergeT

      const mergedTipTs = unique(tipTs.flatMap(T => Object.keys(T)))
        .reduce((acc, key) => {
          // if there is some tip where a field is false, it wins (return false)
          // otherwise return true
          acc[key] = !tipTs.some(T => T[key] === false)
          return acc
        }, {})

      return concat(mergedTipTs, mergeT)
    }
  }

  return {
    schema,
    isValid,
    identity: () => identity,
    concat,
    mapFromInput,
    mapToOutput,
    isConflict,
    isValidMerge,
    merge
  }
}

function ConcatError (T) {
  return new Error(`cannot concat invalid transformation ${JSON.stringify(T)}`)
}

function unique (arr) {
  if (!Array.isArray(arr)) throw Error('function unique expects an array')
  return [...new Set(arr)]
}
