const test = require('tape')
const tangle = require('@tangle/test')

const Strategy = require('../')
const {
  identity,
  isConflict,
  isValidMerge,
  merge
} = Strategy()

test('isConflict', t => {
  // merging stuff! ///////////////////////////
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'complex' })
  const branchGraph = buildGraph(`
    A-->B
    A-->C
  `)
  const A = branchGraph.getNode('A')
  A.data.complex.Alice = true
  A.data.complex.Bob = true
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), false, 'values only on root')

  const B = branchGraph.getNode('B')
  B.data.complex.Alice = false
  B.data.complex.Carol = true
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), false, 'values on root and one branch')

  const C = branchGraph.getNode('C')
  C.data.complex.Carol = true
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), false, 'Comparison shows no difference or identity')
  C.data.complex.Alice = true
  t.equal(isConflict(branchGraph, ['B', 'C'], 'complex'), false, 'still no conflicts')

  t.end()
})

test('isValidMerge', t => {
  // merging stuff! ///////////////////////////
  // NOTE always valid, these tests I just spammed values to try and break it
  // tests copied from another repo :shrug:

  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'complex' })
  const graph = buildGraph(`
    A-->B-->D
    A-->C-->D
  `)
  const mergeNode = graph.getNode('D')

  const A = graph.getNode('A')
  A.data.complex.first = true
  A.data.complex.second = true

  const B = graph.getNode('B')
  B.data.complex.first = false
  B.data.complex.second = true

  const C = graph.getNode('C')
  C.data.complex.second = false

  t.true(isValidMerge(graph, mergeNode, 'complex'), 'no conflict')

  C.data.complex.first = true
  t.true(isValidMerge(graph, mergeNode, 'complex'), 'no conflict')

  mergeNode.data.complex.second = true
  t.true(isValidMerge(graph, mergeNode, 'complex'), 'no conflict')

  mergeNode.data.complex.third = false
  t.true(isValidMerge(graph, mergeNode, 'complex'), 'no conflict')

  mergeNode.data.complex.first = true
  t.true(isValidMerge(graph, mergeNode, 'complex'), 'no conflict')

  t.end()
})

test('simple merge', t => {
  // merging stuff! ///////////////////////////
  // Same example as above, building towards a conflicting graph on one of the fields
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'complex' })
  const graph = buildGraph(`
    A-->B-->D
    A-->C-->D
  `)
  const mergeNode = graph.getNode('D')

  const A = graph.getNode('A')
  A.data.complex.first = true
  A.data.complex.second = true

  const B = graph.getNode('B')
  B.data.complex.first = true
  B.data.complex.second = false

  const C = graph.getNode('C')
  C.data.complex.second = true

  t.deepEqual(merge(graph, mergeNode, 'complex'), { first: true, second: false }, 'identity merge works for no conflict')

  mergeNode.data.complex.second = true
  t.deepEqual(merge(graph, mergeNode, 'complex'), { first: true, second: true }, 'Overwrites conflicting value')

  t.end()
})

test.skip('merging (complex)', t => {
  // Two branches (A and C), then merge three tips
  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'title' })
  const graph = buildGraph(`
    A-->B------>M
    A-->C-->D-->M
        C-->E-->M
   `)
  let mergeNode = graph.getNode('M')

  t.deepEqual(merge(graph, mergeNode, 'title'), identity(), 'merge identities')

  const A = graph.getNode('A')
  A.data.title.animal = 'dog'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'dog' }, 'only root has title')

  const C = graph.getNode('C')
  C.data.title.animal = 'cat'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'cat' }, 'title override in C')

  const D = graph.getNode('D')
  D.data.title.animal = 'mouse'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'mouse' }, 'Tip overwrites earlier branches')

  const E = graph.getNode('E')
  E.data.title.object = 'fridge'
  t.deepEqual(merge(graph, mergeNode, 'title'), { animal: 'mouse', object: 'fridge' }, 'Other nodes dont conflict on other fields')

  E.data.title.animal = 'fridge'
  t.throws(() => merge(graph, mergeNode, 'title'), /cannot merge invalid transformations/, 'do conflict on animal')

  // Two merge nodes E and M in sequence
  const twoMerge = buildGraph(`
   A-->B[{ t: 'cats' }]->D-->M
   A-->C[{ t: 'dogs' }]->F-->M
       B------------------>E-->M
       C------------------>E
    `)
  mergeNode = twoMerge.getNode('E')

  t.equal(isConflict(twoMerge, ['B', 'C'], 'title'), true, 'Conflict resolving E')
  t.equal(isValidMerge(twoMerge, mergeNode, 'title'), false, 'Conflict resolving E')
  t.throws(() => { throw isValidMerge.error }, /Field title is causing a conflict/, 'isValidMerge.error is set')
  t.throws(() => merge(twoMerge, mergeNode, 'title'), /cannot merge invalid transformations/, 'E is invalid merge')
  // t.throws(() => merge(twoMerge, 'M', 'title'), /cannot merge invalid transformations/,
  // 'invalid merge earlier in graph is not caught')  //Actual result: undefined

  mergeNode.data.title = { t: 'either' }
  t.deepEqual(merge(buildGraph(`
  A-->B[{ t: 'cats' }]->D-->M
  A-->C[{ t: 'dogs' }]->F-->M
      B------------------>E-->M
      C------------------>E[{ t: 'either' }]
   `), mergeNode, 'title'), { t: 'either' }, 'earlier merge')

  mergeNode = twoMerge.getNode('M')
  t.deepEqual(merge(buildGraph(`
  A-->B[{ t: 'cats' }]->D-->M
  A-->C[{ t: 'dogs' }]->F-->M
      B------------------>E-->M
      C------------------>E[{ t: 'either' }]
   `), mergeNode, 'title'), { t: 'either' }, 'stacked merges')

  const middleX = buildGraph(`
  A-->B-->D[{ t: 'left' }]--->G
      B-->E-------------------->G-->I
  A-->C-->E-------------------->H-->I
      C-->F[{ t: 'right' }]-->H
   `)
  t.deepEqual(merge(middleX, middleX.getNode('G'), 'title'), { t: 'left' }, 'left merge')
  t.deepEqual(merge(middleX, middleX.getNode('H'), 'title'), { t: 'right' }, 'right merge')
  t.throws(() => merge(middleX, middleX.getNode('I'), 'title'), /cannot merge invalid transformations/, 'left and right conflict')
  t.end()
})
