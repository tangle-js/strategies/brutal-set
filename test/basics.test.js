const test = require('tape')
const { isValid: isValidStrategy } = require('@tangle/strategy')
const Strategy = require('../')

test('Strategy.isValid', t => {
  t.true(isValidStrategy(Strategy()), 'is a valid strategy according to @tangle/strategy')
  if (isValidStrategy.error) console.log(isValidStrategy.error)

  t.end()
})

/* schema/ isValid */
test('schema', t => {
  const { isValid, identity } = Strategy({ keyPattern: '^%[a-zA-Z]+' })

  const Ts = [
    {
      '%profileD': true
    },
    {
      '%profileD': false
    },
    {
      '%profileD': true,
      '%profileX': false
    },
    {},
    identity()
  ]

  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    'dog',
    {
      profile: true
    },
    {
      '%profile': 'dave'
    },
    // {
    //   '%profileD': null
    // },
    { set: null }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})

test('mapFromInput', t => {
  const { mapFromInput } = Strategy()

  t.deepEqual(
    mapFromInput({ A: true }),
    { A: true }
  )

  t.throws(() => mapFromInput('dog'), 'should throw on string')
  t.throws(() => mapFromInput({ A: 'dog' }), 'should throw on value: string')

  t.end()
})

test('mapToOutput', t => {
  const { mapToOutput } = Strategy()

  t.deepEqual(
    mapToOutput({
      A: true,
      B: false
    }),
    ['A']
  )

  t.end()
})

test('concat, identity + associativity', t => {
  const { concat, identity } = Strategy()

  const T = {
    A: true
  }
  t.deepEqual(
    concat(T, identity()),
    T,
    'identity (right)'
  )
  t.deepEqual(
    concat(identity(), T),
    T,
    'identity (left)'
  )

  t.deepEqual(
    concat(
      {
        A: true,
        B: true
      },
      {
        B: false,
        C: true
      }
    ),
    { // expected
      A: true,
      B: false,
      C: true
    },
    'concat'
  )

  let A = { Alice: true }
  let B = { Bob: true }
  let C = { Cherese: false }

  t.deepEqual(
    concat(
      A,
      concat(B, C)
    ),
    concat(
      concat(A, B),
      C
    ),
    'associativity'
  )

  A = { Alice: true }
  B = { Alice: false }
  C = { Alice: true }

  t.deepEqual(
    concat(
      A,
      concat(B, C)
    ),
    concat(
      concat(A, B),
      C
    ),
    'associativity'
  )

  t.end()
})
