const test = require('tape')
const Strategy = require('../')

test('opts.keyPattern', t => {
  const { isValid } = Strategy({
    keyPattern: '^@[a-z]+$'
  })

  const Ts = [
    { '@dog': true },
    { '@dog': true, '@cat': false }
  ]

  Ts.forEach(T => {
    const result = isValid(T)
    t.true(result, `isValid : ${JSON.stringify(T)}`)
    if (!result) console.log('error:', isValid.errors)
  })

  const notTs = [
    { dog: true },
    { dog: true, '@cat': false },
    { '@dog party': true },
    { '@dog6': true }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})
