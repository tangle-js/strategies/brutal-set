# @tangle/brutal-set

This is a fork of the `@tangle/overwrite-fields` which auto-resolves and conflicts such that
set-removal wins.

```js
const T = {
  '@mix': true
  '@colin': false
}
```

Using concat, subfields can be given new values and additional subfields can be added.
```
overwrite.concat(
  {
    '@mix': true,
    '@colin': true
  },
  {
    '@mix': false,
    '@cherese': true
  }
)
// => {
//   '@mix: false,
//   '@colin': true,
//   '@cherese': true,
// }
```

The internal implementation means that once a subfield is set, it cannot be unset.
If you must erase a field you may want to allow values such as `null` and then choose to ignore that.
This strategy is noncommutative, so it conflicts if the same subfield is set to
different values in different branches. A merge is valid if it overwrites all conflicting subfields.
see tests for examples.

## API

### `Overwrite(opts) => overwrite`

`opts` *Object* (optional) can have properties:
- `opts.keyPattern` *String*
    - add a JSON-schema string pattern
    - default: `'^.+$'`

### `overwrite.schema`
### `overwrite.isValid`
### `overwrite.identity() => I`
### `overwrite.concat(R, S) => T`
### `overwrite.mapFromInput(input, currentTips) => T`
### `overwrite.mapToOutput(T) => state`

### `overwrite.isConflict(graph, nodeIds, field) => false`

There is never any conflict in this strategy

### `overwrite.isValidMerge(graph, mergeNode, field) => true`

All merge nodes are valid (as no conflict is defined)

### `overwrite.merge(graph, mergeNode, field) => T`

similar to `isValidMerge`, but return a transformation, `T`
